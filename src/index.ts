import 'reflect-metadata';
import { Server } from './app/server';
import { container } from './inversify.config';

const server = container.get<Server>(Server);
server.listen();