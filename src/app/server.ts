import {inject, injectable} from 'inversify';
import express, {Application, NextFunction, Request, Response} from 'express';
import config from '../config';
import {notFound} from '@hapi/boom';
import { RootRouter } from './routes/Root.router';


@injectable()
export class Server {
    private readonly app: Application;
    constructor(
        @inject(RootRouter) private rootRouter: RootRouter
    ) {
        this.app = express();
        this.app.use('/api', this.rootRouter.getRouter());
        this.app.use((_req: Request, _res: Response, next: NextFunction) => {
            return next(notFound('Not found'));
        });
        this.app.use((error: any, _req: Request, res: Response, _next: NextFunction) => {
            console.error(error);
            res.status(error.output ? error.output.statusCode || 500 : 500).send({error: error.output ? error.output.payload.message : 'Server error'});
        });
    }

    listen() {
        this.app.listen(config.port, () => {
            console.log(`Listening on ${config.port} port`)
        })
    }
}