import {NextFunction, Response, Request} from 'express';

export type middlewareFunction = (req: Request, res: Response, next: NextFunction) => Promise<any>;
