import { Request, Response, NextFunction} from 'express';
import { inject, injectable } from 'inversify';
import {middlewareFunction} from '../interfaces/middleware.interface';
import { AutoReplaceText } from '../../libs/autoReplaceText';
import fs from 'fs';


@injectable()
export class SpellChecker {
    constructor(
        @inject(AutoReplaceText) private AutoReplaceText: AutoReplaceText
    ){
    }
    
    public getCorrectFile: middlewareFunction = async (req: Request, res: Response, next: NextFunction) => {
        const correctText = await this.AutoReplaceText.getCorrectText(req.file.buffer.toString());
        res.setHeader('Content-Type', 'text/plain');
        res.setHeader('Content-Disposition',`attachment; filename=${req.file.originalname}`);
        res.send(correctText);
    }
}