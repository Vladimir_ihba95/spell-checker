import { badRequest } from '@hapi/boom';
import { Multer } from './../../libs/multer';
import { Request, Response, NextFunction} from 'express';
import {inject, injectable} from 'inversify';
import {middlewareFunction} from '../interfaces/middleware.interface';
import { Path } from '../../libs/path';


@injectable()
export class UploadFile {
    constructor(
        @inject(Multer) private multer: Multer,
        @inject(Path) private path: Path
    ) {
    }
    
    public getOneFile(fieldName: string, types: string[], fieldSize: number): middlewareFunction {
        return async (req: Request, res: Response, next: NextFunction) => {
            await this.multer.getOneFileMemoryStorage(fieldName, types, fieldSize)(req, res, next);
            if(!types.includes(this.path.getExt(req.file.originalname))) return next(badRequest(`Допустимые форматы файла: ${types}`)); 
        }
    }
}