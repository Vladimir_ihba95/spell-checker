import {injectable} from 'inversify';
import {Request, Router, Response, NextFunction} from 'express';
import {middlewareFunction} from '../interfaces/middleware.interface';

@injectable()
export abstract class AbstractRouter {

    protected constructor(
        protected router: Router = Router()
    ) {}

    addRouter(uri: string, router: AbstractRouter): void {
        this.router.use(uri, router.getRouter());
    }

    request(type: 'post' | 'get' | 'delete' | 'put', uri: string, ...preMiddleware: middlewareFunction[]): void {
        this.router[type](uri, ...preMiddleware);
    }

    getRouter(): Router {
        return this.router;
    }
}