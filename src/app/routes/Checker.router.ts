import {inject, injectable} from 'inversify';
import {Request, Router, Response, NextFunction} from 'express';
import {AbstractRouter} from './Abstract.router'
import {SpellChecker} from '../middlewares/SpellChecker.middleware'
import {Multer} from '../../libs/multer'
import { UploadFile } from '../middlewares/UploadFile.middleware';


@injectable()
export class Check extends AbstractRouter {
    constructor(
        @inject(SpellChecker) SpellChecker: SpellChecker,
        @inject(UploadFile) UploadFile: UploadFile
    ) {
        super();

        this.request('post', '/', UploadFile.getOneFile('file', ['.txt'], 1000000), SpellChecker.getCorrectFile);
    }
}