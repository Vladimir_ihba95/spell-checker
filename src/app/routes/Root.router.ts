import {inject, injectable} from 'inversify';
import {AbstractRouter} from './Abstract.router';
import {Check} from './Checker.router';

@injectable()
export class RootRouter extends AbstractRouter {
    constructor(
        @inject(Check) private Check: Check
    ) {
        super();
        
        this.addRouter('/checker', this.Check);
    }
}