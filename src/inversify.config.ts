import { Container } from 'inversify';
import { Server } from './app/server';
import { Check } from './app/routes/Checker.router';
import { RootRouter } from './app/routes/Root.router';
import { AbstractRouter } from './app/routes/Abstract.router'
import { SpellChecker } from './app/middlewares/SpellChecker.middleware';
import { Multer } from './libs/multer';
import { UploadFile } from './app/middlewares/UploadFile.middleware';
import { Path } from './libs/path';
import { AutoReplaceText } from './libs/autoReplaceText';

const container = new Container();
container.bind<Server>(Server).toSelf().inSingletonScope();
container.bind<Check>(Check).toSelf().inSingletonScope();
container.bind<AbstractRouter>(AbstractRouter).toSelf().inSingletonScope();
container.bind<RootRouter>(RootRouter).toSelf().inSingletonScope();
container.bind<SpellChecker>(SpellChecker).toSelf().inSingletonScope()
container.bind<Multer>(Multer).toSelf().inSingletonScope()
container.bind<UploadFile>(UploadFile).toSelf().inSingletonScope()
container.bind<Path>(Path).toSelf().inSingletonScope()
container.bind<AutoReplaceText>(AutoReplaceText).toSelf().inSingletonScope()

export {
    container
};