const {PORT} = process.env;
const config = {
    port: Number(PORT) || 3001
}
export default config;