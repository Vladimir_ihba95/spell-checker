import { injectable } from 'inversify';
import path from 'path';


@injectable()
export class Path {
    constructor(){
    }

    public getExt(pathName: string): string {
        return path.extname(pathName);
    }
}