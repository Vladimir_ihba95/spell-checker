import { injectable, inject } from 'inversify';
import multer from 'multer';
import {Request, Router, Response, NextFunction} from 'express';
import {middlewareFunction} from '../app/interfaces/middleware.interface';

@injectable()
export class Multer {
    constructor(
    ) {}

    private memoryStorage(fieldSize: number) {
        const data = {
            storage: multer.memoryStorage(),
            limits: {fieldSize}
        }
        return multer(data)
    }

    public getOneFileMemoryStorage (fieldName: string, types: string[], fieldSize: number) {
        return this.memoryStorage(fieldSize).single(fieldName);
    }
}