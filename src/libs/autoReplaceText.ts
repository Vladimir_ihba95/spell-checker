import { injectable } from 'inversify';
import SpellChecker from 'spellchecker';

@injectable()
export class AutoReplaceText {
    private checker;
    constructor(
    ) {
        this.checker = SpellChecker;
    }

    public async getCorrectText(text: string): Promise<string> {
        let resultText = '';
        let start = 0;
        const errors = await this.checker.checkSpellingAsync(text)
        errors.map((err: {start: number, end: number}, index: number): void => {
            resultText += text.slice(start, err.start);
            const word = text.slice(err.start, err.end);
            const correctWord = this.checker.getCorrectionsForMisspelling(word);
            resultText += correctWord[0] || word;
            start = err.end;
            if(index === errors.length) resultText += text.slice(err.end);
        });
        return resultText;
    }
}